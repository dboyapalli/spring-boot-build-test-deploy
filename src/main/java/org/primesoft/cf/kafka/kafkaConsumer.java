package org.primesoft.cf.kafka;
import java.io.IOException;
import org.primesoft.cf.entity.Forecast;
import org.primesoft.cf.repository.ForecastRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class kafkaConsumer {

	@Autowired
	ForecastRepository forecastRepository;
	private final Logger logger = LoggerFactory.getLogger(getClass());

	@KafkaListener(topics = "test", groupId = "mygroup")
	public void consume(String message) throws IOException {
		ObjectMapper mapper = new ObjectMapper();
		Forecast forecast = mapper.readValue(message, Forecast.class);
		forecastRepository.save(forecast);
		logger.info(String.format("#### -> Consumed message -> %s", message));
	}
}
