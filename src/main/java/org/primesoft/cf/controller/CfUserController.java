package org.primesoft.cf.controller;

import java.net.URI;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import org.primesoft.cf.entity.CFUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("/api/v1")
public class CfUserController {

	@Autowired
	RestTemplate restTemplate;

	private static String url = "http://cff";

	/**
	 * By using GetMapping get all the users
	 */
	@GetMapping("/cfusers")
	public List<Object> getAllUsers() {

		Object[] cfuser = restTemplate.getForObject(url + "/api/v1/cfusers", Object[].class);
		return Arrays.asList(cfuser);
	}

	/**
	 * creating user details using Post Mapping
	 */
	@PostMapping("/cfusers")
	public CFUser createCfUser(@Validated @RequestBody CFUser cfuser) {
		CFUser result = restTemplate.postForObject(url + "/api/v1/cfusers", cfuser, CFUser.class);
		return result;
	}

	/**
	 * Get the user based on userid using Get mapping
	 * 
	 * @throws ResourceNotFoundException
	 */

	@GetMapping("/cfusers/{userId}")
	public CFUser getUserById(@PathVariable(value = "userId") UUID userId) {
		CFUser cfuser = restTemplate.getForObject(url + "/api/v1/cfusers/{userId}", CFUser.class, userId);
		return cfuser;

	}

	/**
	 * delete the user details using Delete Mapping
	 */
	@DeleteMapping("/cfusers/{userId}")
	public String deleteUser(@PathVariable(value = "userId") UUID userId) {

		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		HttpEntity<CFUser> entity = new HttpEntity<CFUser>(headers);
		return restTemplate
				.exchange("http://localhost:8088/api/v1/cfusers/" + userId, HttpMethod.DELETE, entity, String.class)
				.getBody();

	}

	/**
	 * update the user by using PUT mapping
	 */
	@PutMapping("/cfusers/{userId}")
	public String updateUser(@PathVariable(value = "userId") UUID userId, @Validated @RequestBody CFUser userDetails)

	{
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		HttpEntity<CFUser> entity = new HttpEntity<CFUser>(userDetails, headers);
		return restTemplate
				.exchange("http://localhost:8088/api/v1/cfusers/" + userId, HttpMethod.PUT, entity, String.class)
				.getBody();

	}

}
