package org.primesoft.cf.repository;
import java.util.UUID;

import org.primesoft.cf.entity.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface TransactionRepository extends JpaRepository<Transaction, UUID >{
}
