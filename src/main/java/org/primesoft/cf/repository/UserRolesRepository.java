package org.primesoft.cf.repository;
import java.util.UUID;

import org.primesoft.cf.entity.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRolesRepository extends JpaRepository<UserRole, UUID>{
	UserRole findByRole(String role);	
}
